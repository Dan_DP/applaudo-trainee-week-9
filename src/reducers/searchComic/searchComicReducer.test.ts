/* eslint-disable import/extensions */
/* eslint-disable no-undef */
import {
  setComicBaseUrl,
  setComicAllParams,
  setComicTitle,
  setComicFormat,
  setComicLoading,
  setComicError,
  setComicData,
  setComicAsyncContent,
  comicReset,
} from 'actions/searchComic';
import { comicsResponse } from 'mocks/testData';
import SearchComicActions from './searchComic.actions';
import searchComicReducer from './searchComicReducer';

const initState = {
  offset: 0,
  limit: 12,
  baseUrl: '',
  url: '',
  format: '',
  title: '',
  loading: false,
  error: '',
  data: null,
};

describe('Test on searchComicReducer', () => {
  test('should return default values', () => {
    const state = searchComicReducer(initState, {} as SearchComicActions);

    expect(state).toEqual(initState);
  });

  test('should change limit and url', () => {
    const newLimit = 5;
    const state = searchComicReducer(initState, {
      type: 'SET_LIMIT_COMIC',
      payload: {
        limit: newLimit,
      },
    });

    const { limit, url } = state;

    expect(limit).toBe(newLimit);
    expect(url.search(`limit=${newLimit}`)).not.toBe(-1);
    expect(url.search('format=')).toBe(-1);
    expect(url.search('titleStartsWith=')).toBe(-1);
  });

  test('should change offset and url', () => {
    const newOffset = 12;
    const state = searchComicReducer(initState, {
      type: 'SET_OFFSET_COMIC',
      payload: {
        offset: newOffset,
      },
    });

    const { offset, url } = state;

    expect(offset).toBe(newOffset);
    expect(url.search(`offset=${newOffset}`)).not.toBe(-1);
    expect(url.search('format=')).toBe(-1);
    expect(url.search('titleStartsWith=')).toBe(-1);
  });

  test('should set base url', () => {
    const newBaseUrl = 'https://www.google.com';
    const state = searchComicReducer(initState, setComicBaseUrl(newBaseUrl));

    const { url, baseUrl } = state;

    expect(baseUrl).toBe(newBaseUrl);
    expect(url.search(newBaseUrl)).not.toBe(-1);
    expect(url.search('format=')).toBe(-1);
    expect(url.search('titleStartsWith=')).toBe(-1);
  });

  test('should change all query params', () => {
    const newFormat = '1';
    const newOffset = 12;
    const newTitle = 'comic spid';
    const state = searchComicReducer(
      initState,
      setComicAllParams(newOffset, newFormat, newTitle),
    );

    const {
      url,
      offset,
      format,
      title,
    } = state;

    expect(offset).toBe(newOffset);
    expect(format).toBe(newFormat);
    expect(title).toBe(newTitle);
    expect(url.search(`offset=${newOffset}`)).not.toBe(-1);
    expect(url.search(`format=${newFormat}`)).not.toBe(-1);
    expect(url.search(`titleStartsWith=${encodeURI(newTitle)}`)).not.toBe(-1);
  });

  test('should set title', () => {
    const newTitle = 'comic spid';
    const state = searchComicReducer(initState, setComicTitle(newTitle));

    const { title } = state;

    expect(title).toBe(newTitle);
  });

  test('should set format', () => {
    const newFormat = '1';
    const state = searchComicReducer(initState, setComicFormat(newFormat));

    const { format } = state;

    expect(format).toBe(newFormat);
  });

  test('should set loading', () => {
    const newLoading = true;
    const state = searchComicReducer(initState, setComicLoading(newLoading));

    const { loading } = state;

    expect(loading).toBe(newLoading);
  });

  test('should set error', () => {
    const newError = 'Could not load data';
    const state = searchComicReducer(initState, setComicError(newError));

    const { error } = state;

    expect(error).toBe(newError);
  });

  test('should set data', () => {
    const newData = comicsResponse();
    const state = searchComicReducer(initState, setComicData(newData));

    const { data } = state;

    expect(data).toEqual(newData);
  });

  test('should set async content', () => {
    const newData = comicsResponse();
    const newLoading = true;
    const newError = 'error';
    const state = searchComicReducer(
      initState,
      setComicAsyncContent(newLoading, newError, newData),
    );

    const { error, loading, data } = state;

    expect(error).toBe(newError);
    expect(loading).toBe(newLoading);
    expect(data).toEqual(newData);
  });

  test('should reset values', () => {
    const state = searchComicReducer(initState, comicReset());

    expect(state).toEqual(initState);
  });
});
