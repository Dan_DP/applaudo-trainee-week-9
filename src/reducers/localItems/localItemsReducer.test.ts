/* eslint-disable import/extensions */
/* eslint-disable no-undef */
import {
  addBookmark,
  hideLocalItem,
  removeBookmark,
  resetBookmarks,
  resetHiddenItems,
  resetLocalItems,
} from 'actions/localItems';
import LocalItemsActions from './localItems.actions';
import localItemsReducer from './localItemsReducer';
import { ILocalItem } from './ILocalItemsState';

const initState = {
  hiddenItems: [],
  bookmarks: [],
};

describe('Test on localItemsReducer', () => {
  test('should return default values', () => {
    const state = localItemsReducer(initState, {} as LocalItemsActions);

    expect(state).toEqual(initState);
  });

  test('should add hidden item', () => {
    const hiddenItem: ILocalItem = { id: 1, type: 'CHARACTER' };
    const state = localItemsReducer(initState, hideLocalItem(hiddenItem));

    expect(state).toEqual({
      ...initState,
      hiddenItems: [...initState.hiddenItems, hiddenItem],
    });
  });

  test('should add bookmark', () => {
    const bookmark: ILocalItem = { id: 1, type: 'CHARACTER' };
    const state = localItemsReducer(initState, addBookmark(bookmark));

    expect(state).toEqual({
      ...initState,
      bookmarks: [...initState.bookmarks, bookmark],
    });
  });

  test('should remove bookmark', () => {
    const bookmark: ILocalItem = { id: 1, type: 'CHARACTER' };
    const initialBookmarkState = {
      hiddenItems: [],
      bookmarks: [bookmark],
    };
    const state = localItemsReducer(initialBookmarkState, removeBookmark(bookmark));

    expect(state).toEqual(initState);
  });

  test('should reset bookmarks', () => {
    const bookmark: ILocalItem = { id: 1, type: 'CHARACTER' };
    const initialBookmarkState = {
      hiddenItems: [],
      bookmarks: [bookmark],
    };
    const state = localItemsReducer(initialBookmarkState, resetBookmarks());

    expect(state).toEqual(initState);
  });

  test('should reset hidden items', () => {
    const hiddenItem: ILocalItem = { id: 1, type: 'CHARACTER' };
    const initialBookmarkState = {
      hiddenItems: [hiddenItem],
      bookmarks: [],
    };
    const state = localItemsReducer(initialBookmarkState, resetHiddenItems());

    expect(state).toEqual(initState);
  });

  test('should reset all local items', () => {
    const hiddenItem: ILocalItem = { id: 1, type: 'CHARACTER' };
    const bookmark: ILocalItem = { id: 2, type: 'COMIC' };
    const initialBookmarkState = {
      hiddenItems: [hiddenItem],
      bookmarks: [bookmark],
    };
    const state = localItemsReducer(initialBookmarkState, resetLocalItems());

    expect(state).toEqual(initState);
  });
});
