/* eslint-disable no-undef */
/* eslint-disable import/extensions */
import {
  setStoryBaseUrl,
  setStoryAllParams,
  setStoryTitle,
  setStoryLoading,
  setStoryError,
  setStoryData,
  setStoryAsyncContent,
  storyReset,
} from 'actions/searchStory';
import { storiesResponse } from 'mocks/testData';
import SearchStoryActions from './searchStory.actions';
import searchStoryReducer from './searchStoryReducer';

const initState = {
  offset: 0,
  limit: 12,
  baseUrl: '',
  url: '',
  title: '',
  loading: false,
  error: '',
  data: null,
};

describe('Test on searchStoryReducer', () => {
  test('should return default values', () => {
    const state = searchStoryReducer(initState, {} as SearchStoryActions);

    expect(state).toEqual(initState);
  });

  test('should change limit and url', () => {
    const newLimit = 5;
    const state = searchStoryReducer(initState, {
      type: 'SET_LIMIT_STORY',
      payload: {
        limit: newLimit,
      },
    });

    const { limit, url } = state;

    expect(limit).toBe(newLimit);
    expect(url.search(`limit=${newLimit}`)).not.toBe(-1);
    expect(url.search('titleStartsWith=')).toBe(-1);
  });

  test('should change offset and url', () => {
    const newOffset = 12;
    const state = searchStoryReducer(initState, {
      type: 'SET_OFFSET_STORY',
      payload: {
        offset: newOffset,
      },
    });

    const { offset, url } = state;

    expect(offset).toBe(newOffset);
    expect(url.search(`offset=${newOffset}`)).not.toBe(-1);
    expect(url.search('titleStartsWith=')).toBe(-1);
  });

  test('should set base url', () => {
    const newBaseUrl = 'https://www.google.com';
    const state = searchStoryReducer(initState, setStoryBaseUrl(newBaseUrl));

    const { url, baseUrl } = state;

    expect(baseUrl).toBe(newBaseUrl);
    expect(url.search(newBaseUrl)).not.toBe(-1);
    expect(url.search('titleStartsWith=')).toBe(-1);
  });

  test('should change all query params', () => {
    const newOffset = 12;
    const newTitle = 'story spid';
    const state = searchStoryReducer(
      initState,
      setStoryAllParams(newOffset, newTitle),
    );

    const {
      url,
      offset,
      title,
    } = state;

    expect(offset).toBe(newOffset);
    expect(title).toBe(newTitle);
    expect(url.search(`offset=${newOffset}`)).not.toBe(-1);
    expect(url.search(`titleStartsWith=${encodeURI(newTitle)}`)).not.toBe(-1);
  });

  test('should set title', () => {
    const newTitle = 'comic spid';
    const state = searchStoryReducer(initState, setStoryTitle(newTitle));

    const { title } = state;

    expect(title).toBe(newTitle);
  });

  test('should set loading', () => {
    const newLoading = true;
    const state = searchStoryReducer(initState, setStoryLoading(newLoading));

    const { loading } = state;

    expect(loading).toBe(newLoading);
  });

  test('should set error', () => {
    const newError = 'Could not load data';
    const state = searchStoryReducer(initState, setStoryError(newError));

    const { error } = state;

    expect(error).toBe(newError);
  });

  test('should set data', () => {
    const newData = storiesResponse();
    const state = searchStoryReducer(initState, setStoryData(newData));

    const { data } = state;

    expect(data).toEqual(newData);
  });

  test('should set async content', () => {
    const newData = storiesResponse();
    const newLoading = true;
    const newError = 'error';
    const state = searchStoryReducer(
      initState,
      setStoryAsyncContent(newLoading, newError, newData),
    );

    const { error, loading, data } = state;

    expect(error).toBe(newError);
    expect(loading).toBe(newLoading);
    expect(data).toEqual(newData);
  });

  test('should reset values', () => {
    const state = searchStoryReducer(initState, storyReset());

    expect(state).toEqual(initState);
  });
});
