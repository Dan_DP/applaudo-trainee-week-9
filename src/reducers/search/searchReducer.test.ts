/* eslint-disable import/extensions */
/* eslint-disable no-undef */
import {
  setAllParams,
  setBaseUrl,
  setComic,
  setData,
  setError,
  setLoading,
  setName,
  setOffset,
  setStory,
  setAsyncContent,
  reset,
} from 'actions/search';
import { charactersResponse } from 'mocks/testData';
import SearchActions from './search.actions';
import searchReducer from './searchReducer';

const initState = {
  offset: 0,
  limit: 12,
  baseUrl: '',
  url: '',
  name: '',
  comic: '',
  story: '',
  loading: false,
  error: '',
  data: null,
};

describe('Test on searchReducer', () => {
  test('should return default values', () => {
    const state = searchReducer(initState, {} as SearchActions);

    expect(state).toEqual(initState);
  });

  test('should change limit and url', () => {
    const newLimit = 5;
    const state = searchReducer(initState, {
      type: 'SET_LIMIT',
      payload: {
        limit: newLimit,
      },
    });

    const { limit, url } = state;

    expect(limit).toBe(newLimit);
    expect(url.search(`limit=${newLimit}`)).not.toBe(-1);
    expect(url.search('nameStartsWith=')).toBe(-1);
    expect(url.search('comics=')).toBe(-1);
    expect(url.search('stories=')).toBe(-1);
  });

  test('should change offset and url', () => {
    const newOffset = 5;
    const state = searchReducer(initState, setOffset(newOffset));

    const { offset, url } = state;

    expect(offset).toBe(newOffset);
    expect(url.search(`offset=${newOffset}`)).not.toBe(-1);
    expect(url.search('nameStartsWith=')).toBe(-1);
    expect(url.search('comics=')).toBe(-1);
    expect(url.search('stories=')).toBe(-1);
  });

  test('should set base url', () => {
    const newBaseUrl = 'https://www.google.com';
    const state = searchReducer(initState, setBaseUrl(newBaseUrl));

    const { url, baseUrl } = state;

    expect(baseUrl).toBe(newBaseUrl);
    expect(url.search(newBaseUrl)).not.toBe(-1);
    expect(url.search('nameStartsWith=')).toBe(-1);
    expect(url.search('comics=')).toBe(-1);
    expect(url.search('stories=')).toBe(-1);
  });

  test('should change all query params', () => {
    const newName = 'spid hero';
    const newOffset = 12;
    const newComic = '5';
    const newStory = '1';
    const state = searchReducer(initState, setAllParams(newOffset, newName, newComic, newStory));

    const {
      url,
      offset,
      name,
      comic,
      story,
    } = state;

    expect(offset).toBe(newOffset);
    expect(name).toBe(newName);
    expect(comic).toBe(newComic);
    expect(story).toBe(newStory);
    expect(url.search(`offset=${newOffset}`)).not.toBe(-1);
    expect(url.search(`nameStartsWith=${encodeURI(newName)}`)).not.toBe(-1);
    expect(url.search(`comics=${newComic}`)).not.toBe(-1);
    expect(url.search(`stories=${newStory}`)).not.toBe(-1);
  });

  test('should set name', () => {
    const newName = 'spid';
    const state = searchReducer(initState, setName(newName));

    const { name } = state;

    expect(name).toBe(newName);
  });

  test('should set comic', () => {
    const newComic = '1';
    const state = searchReducer(initState, setComic(newComic));

    const { comic } = state;

    expect(comic).toBe(newComic);
  });

  test('should set story', () => {
    const newStory = '1';
    const state = searchReducer(initState, setStory(newStory));

    const { story } = state;

    expect(story).toBe(newStory);
  });

  test('should set loading', () => {
    const newLoading = true;
    const state = searchReducer(initState, setLoading(newLoading));

    const { loading } = state;

    expect(loading).toBe(newLoading);
  });

  test('should set error', () => {
    const newError = 'Could not load data';
    const state = searchReducer(initState, setError(newError));

    const { error } = state;

    expect(error).toBe(newError);
  });

  test('should set data', () => {
    const newData = charactersResponse();
    const state = searchReducer(initState, setData(newData));

    const { data } = state;

    expect(data).toEqual(newData);
  });

  test('should set async content', () => {
    const newData = charactersResponse();
    const newLoading = true;
    const newError = 'error';
    const state = searchReducer(initState, setAsyncContent(newLoading, newError, newData));

    const { error, loading, data } = state;

    expect(error).toBe(newError);
    expect(loading).toBe(newLoading);
    expect(data).toEqual(newData);
  });

  test('should reset values', () => {
    const state = searchReducer(initState, reset());

    expect(state).toEqual(initState);
  });
});
