/* eslint-disable import/extensions */
/* eslint-disable no-undef */
import {
  setViewItemData,
  setViewItemError,
  setViewItemLoading,
  setViewItemAsyncContent,
  viewItemReset,
} from 'actions/viewItem';
import { storiesResponse } from 'mocks/testData';
import ViewItemActions from './viewItem.actions';
import viewItemReducer from './viewItemReducer';

const initState = {
  loading: false,
  error: '',
  data: null,
};

describe('Test on viewItemReducer', () => {
  test('should return default values', () => {
    const state = viewItemReducer(initState, {} as ViewItemActions);

    expect(state).toEqual(initState);
  });

  test('should set loading', () => {
    const newLoading = true;
    const state = viewItemReducer(initState, setViewItemLoading(newLoading));

    const { loading } = state;

    expect(loading).toBe(newLoading);
  });

  test('should set error', () => {
    const newError = 'Could not load data';
    const state = viewItemReducer(initState, setViewItemError(newError));

    const { error } = state;

    expect(error).toBe(newError);
  });

  test('should set data', () => {
    const newData = storiesResponse();
    const state = viewItemReducer(initState, setViewItemData(newData));

    const { data } = state;

    expect(data).toEqual(newData);
  });

  test('should set async content', () => {
    const newData = storiesResponse();
    const newLoading = true;
    const newError = 'error';
    const state = viewItemReducer(
      initState,
      setViewItemAsyncContent(newLoading, newError, newData),
    );

    const { error, loading, data } = state;

    expect(error).toBe(newError);
    expect(loading).toBe(newLoading);
    expect(data).toEqual(newData);
  });

  test('should reset values', () => {
    const state = viewItemReducer(initState, viewItemReset());

    expect(state).toEqual(initState);
  });
});
