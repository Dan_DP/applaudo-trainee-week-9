/* eslint-disable no-undef */
import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';

import App from 'components/App/App';

describe('Test on index file', () => {
  test('should render correctly', async () => {
    const { container } = render(<App />);

    await waitFor(() => expect(screen.getByText(/Stan Lee/i)).toBeInTheDocument());

    expect(container).toMatchSnapshot();
  });
});
