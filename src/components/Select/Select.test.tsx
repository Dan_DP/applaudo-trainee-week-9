/* eslint-disable no-undef */
/* eslint-disable import/extensions */
import React from 'react';
import { fireEvent, render } from '@testing-library/react';
import Select from './Select';

describe('Test on Select component', () => {
  const items = [
    {
      id: 1,
      title: 'spiderman',
    },
    {
      id: 2,
      title: 'ironman',
    },
  ];
  const defaultOptionText = 'Select a character';
  const value = '';
  const handleChange = jest.fn();

  test('should render component correctly', () => {
    const { container } = render(
      <Select
        items={items}
        defaultOptionText={defaultOptionText}
        value={value}
        onChange={handleChange}
      />,
    );

    expect(container).toMatchSnapshot();
  });

  test('should handle change correctly', () => {
    const { getByTestId } = render(
      <Select
        items={items}
        defaultOptionText={defaultOptionText}
        value={value}
        onChange={handleChange}
      />,
    );

    fireEvent.change(getByTestId('select'), { target: { value: 2 } });

    expect(handleChange).toHaveBeenCalledTimes(1);
  });
});
