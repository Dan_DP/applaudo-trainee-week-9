/* eslint-disable no-undef */
/* eslint-disable import/extensions */
import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import ConfirmModal from './ConfirmModal';

describe('Test on ConfirmModal', () => {
  const titleModal = 'modal title';
  const confirmTextModal = 'Confirm';
  const showModal = true;
  const handleAction = jest.fn();

  beforeEach(() => {
    jest.clearAllMocks();
    let modalRoot = document.getElementById('modal-portal');
    if (!modalRoot) {
      modalRoot = document.createElement('div');
      modalRoot.setAttribute('id', 'modal-portal');
      document.body.appendChild(modalRoot);
    }
  });

  test('should trigger handle action with confirm', async () => {
    render(
      <ConfirmModal
        title={titleModal}
        confirmText={confirmTextModal}
        open={showModal}
        handleAction={handleAction}
      >
        <p>Body modal</p>
      </ConfirmModal>,
    );

    await waitFor(() => {
      expect(screen.getByText(/Cancel/i)).toBeInTheDocument();
    });

    userEvent.click(screen.getByText(confirmTextModal));
    expect(handleAction).toHaveBeenCalledTimes(1);
    expect(handleAction).toHaveBeenCalledWith(true);
  });

  test('should trigger handle action with cancel', async () => {
    render(
      <ConfirmModal
        title={titleModal}
        confirmText={confirmTextModal}
        open={showModal}
        handleAction={handleAction}
      >
        <p>Body modal</p>
      </ConfirmModal>,
    );

    await waitFor(() => {
      expect(screen.getByText(/Cancel/i)).toBeInTheDocument();
    });

    userEvent.click(screen.getByText(/Cancel/i));
    expect(handleAction).toHaveBeenCalledTimes(1);
    expect(handleAction).toHaveBeenCalledWith(false);
  });

  test('should trigger handle action with cross button', async () => {
    render(
      <ConfirmModal
        title={titleModal}
        confirmText={confirmTextModal}
        open={showModal}
        handleAction={handleAction}
      >
        <p>Body modal</p>
      </ConfirmModal>,
    );

    await waitFor(() => {
      expect(screen.getByText(/Cancel/i)).toBeInTheDocument();
    });

    // userEvent.click(container.querySelector('.close') as TargetElement);
    userEvent.click(screen.getByTestId('close'));
    expect(handleAction).toHaveBeenCalledTimes(1);
    expect(handleAction).toHaveBeenCalledWith(false);
  });
});
