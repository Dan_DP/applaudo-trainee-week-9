/* eslint-disable import/extensions */
/* eslint-disable no-undef */
import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import CustomOrderedList from './CustomOrderedList';

describe('Test on CustomOrderedList', () => {
  const items = [
    {
      resourceURI: 'characters/1',
      name: 'spiderman',
    },
    {
      resourceURI: 'characters/2',
      name: 'ironman',
    },
  ];
  const handleClick = jest.fn();

  beforeEach(() => {
    jest.clearAllMocks();
  });

  test('should render component correctly', () => {
    const { container } = render(
      <CustomOrderedList items={items} onClick={handleClick} />,
    );

    expect(container).toMatchSnapshot();
  });

  test('should trigger onClick with correct variable', () => {
    render(<CustomOrderedList items={items} onClick={handleClick} />);

    userEvent.click(screen.getByText(/spiderman/i));

    expect(handleClick).toHaveBeenCalledTimes(1);
    expect(handleClick).toHaveBeenCalledWith('1');
  });
});
