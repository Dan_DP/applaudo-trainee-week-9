/* eslint-disable import/extensions */
/* eslint-disable no-undef */
import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent, { TargetElement } from '@testing-library/user-event';
import { faBookmark } from '@fortawesome/free-solid-svg-icons';

import Card from './Card';

describe('Test on Card component', () => {
  const handleViewMore = jest.fn();
  const handleHideItem = jest.fn();
  const handleBookmarkAction = jest.fn();
  const className = 'btn btn-action btn-bookmark';
  const data = {
    id: 1,
    name: 'spiderman',
    description: 'description',
    thumbnail: {
      path: 'path',
      extension: 'jpg',
    },
    modified: new Date(),
    resourceURI: '',
    urls: [],
    comics: {} as any,
    stories: {} as any,
    events: {} as any,
    series: {} as any,
  };

  beforeEach(() => {
    jest.clearAllMocks();
  });

  test('should render component correctly', () => {
    const { container } = render(
      <Card
        id={data.id}
        name={data.name}
        description={data.description}
        bookmarkClassName={className}
        bookmarkIcon={faBookmark}
        handleViewMore={handleViewMore}
        handleHideItem={handleHideItem}
        handleBookmarkAction={handleBookmarkAction}
        imageUrl={`${data.thumbnail.path}/portrait_uncanny.${data.thumbnail.extension}`}
      />,
    );

    expect(container).toMatchSnapshot();
  });

  test('should call handle view more function', () => {
    render(
      <Card
        id={data.id}
        name={data.name}
        description={data.description}
        bookmarkClassName={className}
        bookmarkIcon={faBookmark}
        handleViewMore={handleViewMore}
        handleHideItem={handleHideItem}
        handleBookmarkAction={handleBookmarkAction}
        imageUrl={`${data.thumbnail.path}/portrait_uncanny.${data.thumbnail.extension}`}
      />,
    );

    userEvent.click(screen.getByText(/View More/i));

    expect(handleViewMore).toHaveBeenCalledTimes(1);
    expect(handleViewMore).toHaveBeenCalledWith(data.id);
  });

  test('should call handle bookmark function', () => {
    const { container } = render(
      <Card
        id={data.id}
        name={data.name}
        description={data.description}
        bookmarkClassName={className}
        bookmarkIcon={faBookmark}
        handleViewMore={handleViewMore}
        handleHideItem={handleHideItem}
        handleBookmarkAction={handleBookmarkAction}
        imageUrl={`${data.thumbnail.path}/portrait_uncanny.${data.thumbnail.extension}`}
      />,
    );

    userEvent.click(container.querySelector('.btn-bookmark') as TargetElement);

    expect(handleBookmarkAction).toHaveBeenCalledTimes(1);
    expect(handleBookmarkAction).toHaveBeenCalledWith(data.id);
  });

  test('should call handle hide item function', () => {
    const { container } = render(
      <Card
        id={data.id}
        name={data.name}
        description={data.description}
        bookmarkClassName={className}
        bookmarkIcon={faBookmark}
        handleViewMore={handleViewMore}
        handleHideItem={handleHideItem}
        handleBookmarkAction={handleBookmarkAction}
        imageUrl={`${data.thumbnail.path}/portrait_uncanny.${data.thumbnail.extension}`}
      />,
    );

    userEvent.click(container.querySelector('.btn-hide') as TargetElement);

    expect(handleHideItem).toHaveBeenCalledTimes(1);
    expect(handleHideItem).toHaveBeenCalledWith(data.id);
  });
});
