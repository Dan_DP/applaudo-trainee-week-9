/* eslint-disable import/extensions */
/* eslint-disable no-undef */
import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { multipleResultsResponse } from 'mocks/testData';
import Pagination from './Pagination';

describe('Tests on Pagination component', () => {
  const genericResponse = multipleResultsResponse();
  const handleChange = jest.fn();

  beforeEach(() => {
    jest.clearAllMocks();
  });

  test('should render component correctly', () => {
    const { container } = render(
      <Pagination genericResponse={genericResponse} onChange={handleChange} />,
    );

    expect(container).toMatchSnapshot();
  });

  test('should trigger first button', () => {
    render(
      <Pagination genericResponse={genericResponse} onChange={handleChange} />,
    );

    userEvent.click(screen.getByText(/First/i));

    expect(handleChange).toHaveBeenCalledTimes(1);
    expect(handleChange).toHaveBeenCalledWith(1);
  });

  test('should trigger prev button', () => {
    const { limit, offset } = genericResponse.data;
    const newOffset = offset - limit;
    const newPage = Math.ceil(newOffset / limit + 1);
    render(
      <Pagination genericResponse={genericResponse} onChange={handleChange} />,
    );

    userEvent.click(screen.getByText(/Prev/i));

    expect(handleChange).toHaveBeenCalledTimes(1);
    expect(handleChange).toHaveBeenCalledWith(newPage);
  });

  test('should trigger next button', () => {
    const { limit, offset } = genericResponse.data;
    const newOffset = offset + limit;
    const newPage = Math.ceil(newOffset / limit + 1);
    render(
      <Pagination genericResponse={genericResponse} onChange={handleChange} />,
    );

    userEvent.click(screen.getByText(/Next/i));

    expect(handleChange).toHaveBeenCalledTimes(1);
    expect(handleChange).toHaveBeenCalledWith(newPage);
  });

  test('should trigger last button', () => {
    const { limit, total } = genericResponse.data;
    const newOffset = total % limit === 0 ? total - limit : total - (total % limit);
    const newPage = Math.ceil(newOffset / limit + 1);
    render(
      <Pagination genericResponse={genericResponse} onChange={handleChange} />,
    );

    userEvent.click(screen.getByText(/Last/i));

    expect(handleChange).toHaveBeenCalledTimes(1);
    expect(handleChange).toHaveBeenCalledWith(newPage);
  });
});
