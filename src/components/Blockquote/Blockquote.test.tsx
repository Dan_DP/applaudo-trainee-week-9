/* eslint-disable no-undef */
/* eslint-disable import/extensions */
import React from 'react';
import { render } from '@testing-library/react';

import Blockquote from './Blockquote';

describe('Test on Blockquote component', () => {
  const author = 'Stan Lee';
  const quote = 'Roses are red, violets are blue';

  test('should render component correctly', () => {
    const { container } = render(<Blockquote author={author} quote={quote} />);

    expect(container).toMatchSnapshot();
  });
});
