/* eslint-disable import/extensions */
/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-undef */
import { renderHook, act } from '@testing-library/react-hooks';

import useWindowDimensions from './useWindowDimensions';

describe('Test on useWindowDimensions hook', () => {
  test('should return the correct width and height', () => {
    const newWidth = 900;
    const newHeight = 500;
    const { result } = renderHook(() => useWindowDimensions());

    act(() => {
      global.innerWidth = newWidth;
      global.innerHeight = newHeight;
      global.dispatchEvent(new Event('resize'));
    });

    const { height, width } = result.current;

    expect(width).toBe(newWidth);
    expect(height).toBe(newHeight);
  });
});
