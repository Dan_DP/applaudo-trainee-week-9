/* eslint-disable import/extensions */
/* eslint-disable no-undef */
/* eslint-disable import/no-extraneous-dependencies */
import { renderHook, act } from '@testing-library/react-hooks';
import { ChangeEvent } from 'react';

import useForm from './useForm';

describe('Test on useForm hook', () => {
  const initialForm = {
    name: 'Vicente',
    email: 'vicente@gmail.com',
  };

  test('should return a default form', () => {
    const { result } = renderHook(() => useForm(initialForm));
    const [values, handleInputChange, reset] = result.current;

    expect(values).toEqual(initialForm);
    expect(typeof handleInputChange).toBe('function');
    expect(typeof reset).toBe('function');
  });

  test('should change form value', () => {
    const modifiedFormName = {
      target: {
        name: 'name',
        value: 'Daniel',
      },
    };
    const { result } = renderHook(() => useForm(initialForm));
    const [, handleInputChange] = result.current;

    act(() => {
      handleInputChange(modifiedFormName as ChangeEvent<HTMLInputElement>);
    });

    const [values] = result.current;

    expect(values).toEqual({ ...initialForm, name: 'Daniel' });
  });

  test('should reset form value with reset', () => {
    const modifiedFormName = {
      target: {
        name: 'name',
        value: 'Daniel',
      },
    };
    const { result } = renderHook(() => useForm(initialForm));
    const [, handleInputChange, reset] = result.current;

    act(() => {
      handleInputChange(modifiedFormName as ChangeEvent<HTMLInputElement>);
      reset();
    });

    const [values] = result.current;

    expect(values).toEqual(initialForm);
  });
});
