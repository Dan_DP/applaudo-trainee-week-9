/* eslint-disable arrow-body-style */
/* eslint-disable import/extensions */
/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-undef */
import { renderHook } from '@testing-library/react-hooks';
import { rest } from 'msw';

import server from 'mocks/server';
import ICharacter from 'interfaces/ICharacter';
import IGenericApiResponse from 'interfaces/IGenericApiResponse';
import useFetch from './useFetch';

describe('Test on useFetch hook', () => {
  test('should return default values', () => {
    const { result } = renderHook(() => useFetch(`${process.env.REACT_APP_API_URL}v1/public/characters`));

    const { data, loading, error } = result.current;

    expect(data).toBeNull();
    expect(loading).toBe(true);
    expect(error).toBeNull();
  });

  test('should get api values', async () => {
    const { result, waitForNextUpdate } = renderHook(() => useFetch<IGenericApiResponse<ICharacter>>(`${process.env.REACT_APP_API_URL}v1/public/characters`));

    await waitForNextUpdate();

    const { data, loading, error } = result.current;

    expect(data?.status).toBe('ok');
    expect(loading).toBe(false);
    expect(error).toBeNull();
  });

  test('should handle error', async () => {
    server.use(
      rest.get(`${process.env.REACT_APP_API_URL}v1/public/characters`, (req, res, ctx) => {
        return res(
          ctx.status(404),
        );
      }),
    );

    const { result, waitForNextUpdate } = renderHook(() => useFetch<IGenericApiResponse<ICharacter>>(`${process.env.REACT_APP_API_URL}v1/public/characters`));

    await waitForNextUpdate();

    const { data, loading, error } = result.current;

    expect(data).toBeNull();
    expect(loading).toBe(false);
    expect(error).toBe('Can not load info');
  });
});
