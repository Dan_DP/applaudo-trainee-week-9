/* eslint-disable no-undef */
/* eslint-disable import/no-extraneous-dependencies */
import { screen, waitFor } from '@testing-library/react';
import userEvent, { TargetElement } from '@testing-library/user-event';
import { createMemoryHistory } from 'history';

import { renderWithCustomHistory } from 'tests/componentUtils';

describe('Test on StoryBookmarks', () => {
  test('should show bookmarks', async () => {
    const history = createMemoryHistory();
    history.push('/stories');
    const { container } = renderWithCustomHistory(history);

    await waitFor(() => {
      expect(container.querySelector('.card')).not.toBeNull();
      expect(container.querySelector('.loading-container')).toBeNull();
    });

    userEvent.click(container.querySelector('.btn-bookmark') as TargetElement);
    userEvent.click(screen.getAllByText(/Bookmarks/i)[0]);

    await waitFor(() => {
      expect(screen.getByText(/Character Bookmarks/i)).not.toBeNull();
    });

    userEvent.click(screen.getAllByText(/Stories/i)[1]);

    await waitFor(() => {
      expect(screen.getByText(/Story Bookmarks/i)).not.toBeNull();
      expect(screen.getByText(/story spiderman/i)).not.toBeNull();
    });
  });

  test('should remove bookmark with card button', async () => {
    const history = createMemoryHistory();
    history.push('/bookmarks/stories');
    const { container } = renderWithCustomHistory(history);

    await waitFor(() => {
      expect(screen.getByText(/Story Bookmarks/i)).not.toBeNull();
      expect(container.querySelector('.card')).not.toBeNull();
      expect(container.querySelector('.loading-container')).toBeNull();
    });

    userEvent.click(container.querySelector('.btn-bookmark') as TargetElement);
    expect(container.querySelector('.card')).toBeNull();
  });

  test('should remove bookmarks with Remove all bookmarks button', async () => {
    let modalRoot = document.getElementById('modal-portal');
    if (!modalRoot) {
      modalRoot = document.createElement('div');
      modalRoot.setAttribute('id', 'modal-portal');
      document.body.appendChild(modalRoot);
    }
    const history = createMemoryHistory();
    history.push('/stories');
    const { container } = renderWithCustomHistory(history);

    await waitFor(() => {
      expect(container.querySelector('.card')).not.toBeNull();
      expect(container.querySelector('.loading-container')).toBeNull();
    });

    userEvent.click(container.querySelector('.btn-bookmark') as TargetElement);
    userEvent.click(screen.getAllByText(/Bookmarks/i)[0]);

    await waitFor(() => {
      expect(screen.getByText(/Character Bookmarks/i)).not.toBeNull();
    });

    userEvent.click(screen.getAllByText(/Stories/i)[1]);

    await waitFor(() => {
      expect(screen.getByText(/Story Bookmarks/i)).not.toBeNull();
      expect(screen.getByText(/story spiderman/i)).not.toBeNull();
    });

    userEvent.click(container.querySelector('.delete-bookmarks') as TargetElement);
    await waitFor(() => {
      expect(screen.getByText(/Are you sure you want to delete all of your bookmarks in characters,comics and stories?/i)).not.toBeNull();
    });

    userEvent.click(screen.getByText(/Yes, delete!/i));
    expect(container.querySelector('.card')).toBeNull();
  });

  test('should hide bookmark', async () => {
    const history = createMemoryHistory();
    history.push('/stories');
    const { container } = renderWithCustomHistory(history);

    await waitFor(() => {
      expect(container.querySelector('.card')).not.toBeNull();
      expect(container.querySelector('.loading-container')).toBeNull();
    });

    userEvent.click(container.querySelector('.btn-bookmark') as TargetElement);
    userEvent.click(screen.getAllByText(/Bookmarks/i)[0]);

    await waitFor(() => {
      expect(screen.getByText(/Character Bookmarks/i)).not.toBeNull();
    });

    userEvent.click(screen.getAllByText(/Stories/i)[1]);

    await waitFor(() => {
      expect(screen.getByText(/Story Bookmarks/i)).not.toBeNull();
      expect(screen.getByText(/story spiderman/i)).not.toBeNull();
    });

    userEvent.click(container.querySelector('.btn-hide') as TargetElement);
    expect(container.querySelector('.card')).toBeNull();
  });

  test('should reset all hidden items', async () => {
    const mockResponse = jest.fn();
    Object.defineProperty(window, 'location', {
      value: {
        reload: mockResponse,
      },
      writable: true,
    });

    let modalRoot = document.getElementById('modal-portal');
    if (!modalRoot) {
      modalRoot = document.createElement('div');
      modalRoot.setAttribute('id', 'modal-portal');
      document.body.appendChild(modalRoot);
    }

    const history = createMemoryHistory();
    history.push('/bookmarks/stories');
    const { container } = renderWithCustomHistory(history);

    await waitFor(() => {
      expect(screen.getByText(/Story Bookmarks/i)).not.toBeNull();
      expect(container.querySelector('.card')).toBeNull();
    });

    userEvent.click(container.querySelector('.reset-hidden-items') as TargetElement);
    await waitFor(() => {
      expect(screen.getByText(/Are you sure you want to reset all hidden items?/i)).not.toBeNull();
    });

    userEvent.click(screen.getByText(/Yes!/i));
    expect(mockResponse).toHaveBeenCalledTimes(1);
  });
});
